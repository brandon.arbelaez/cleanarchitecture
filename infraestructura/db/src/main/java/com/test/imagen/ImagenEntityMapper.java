package com.test.imagen;

import com.test.imagen.entity.DAO.ImagenDAO;
import com.test.persona.PersonaModel;
import com.test.persona.entity.DAO.PersonaDAO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ImagenEntityMapper {

    ImagenModel imagenEntityToPersonaModel(ImagenDAO imagenEntity);

    ImagenDAO imagenModelToPersonaEntity(ImagenModel imagenModel);

    List<ImagenModel> imagenEntityToPersonaModel(List<ImagenDAO> imagenEntity);
}
