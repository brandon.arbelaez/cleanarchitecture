package com.test.imagen.repository;

import com.google.common.collect.Streams;
import com.test.imagen.ImagenEntityMapper;
import com.test.imagen.ImagenModel;
import com.test.imagen.entity.DAO.ImagenDAO;
import com.test.imagen.gateway.ImagenGateway;
import com.test.imagen.repository.JPA.ImagenRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

@Repository
@RequiredArgsConstructor
public class ImagenAdapter implements ImagenGateway {
    private final ImagenRepository imagenRepository;
    private final ImagenEntityMapper imagenEntityMapper;

    @Override
    public List<ImagenModel> findAll() {
        List<ImagenDAO> imagenEntities = Streams
                .stream(imagenRepository.findAll())
                .collect(Collectors.toList());
        return imagenEntityMapper.imagenEntityToPersonaModel(imagenEntities);
    }

    @Override
    public ImagenModel findByPersonaid(String id) {
        ImagenDAO byPersonaId = imagenRepository.findByPersonaid(id);
        return imagenEntityMapper.imagenEntityToPersonaModel(byPersonaId);
    }

    @Override
    public Integer deleteByPersonaid(String id) {
        Integer delete= imagenRepository.deleteByPersonaid(id);
        return delete;
    }
}
