package com.test.imagen.entity.DTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class ImagenDTO {
    private String _id;

    private String personaid;

    private String descripcionFoto;
}
