package com.test.imagen.entity.DAO;

import lombok.Data;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@Document(value = "IMAGEN")
public class ImagenDAO {
    @Id
    private String _id;

    private String personaid;

    private String descripcionFoto;
}
