package com.test.imagen.repository.JPA;

import com.test.imagen.entity.DAO.ImagenDAO;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface ImagenRepository extends MongoRepository<ImagenDAO, String> {
    ImagenDAO findByPersonaid(String id);

    Integer deleteByPersonaid(String id);
}
