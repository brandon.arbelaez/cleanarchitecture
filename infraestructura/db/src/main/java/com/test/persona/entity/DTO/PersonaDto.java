package com.test.persona.entity.DTO;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.test.imagen.entity.DAO.ImagenDAO;
import com.test.imagen.entity.DTO.ImagenDTO;
import lombok.Data;

/**
 * @author pedro
 */
@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class PersonaDto {

    private Long id;
    private String nombre;
    private String apellido;
    private Integer identificacion;
    private Integer tipoDocumento;
    private Integer edad;
    private Integer ciudad;
    private ImagenDTO imagen;

}
