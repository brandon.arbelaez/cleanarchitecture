package com.test.persona;

import com.test.persona.entity.DAO.PersonaDAO;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author pedro
 */
@Mapper(componentModel = "spring")
public interface PersonaEntityMapper {

    PersonaModel personaEntityToPersonaModel(PersonaDAO personaEntity);

    PersonaDAO personaModelToPersonaEntity(PersonaModel personaModel);

    List<PersonaModel> personaEntityToPersonaModel(List<PersonaDAO> personaEntity);
}
