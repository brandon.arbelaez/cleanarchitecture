package com.test.persona.repository.JPA;

import com.test.persona.PersonaModel;
import com.test.persona.entity.DAO.PersonaDAO;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.List;

/**
 * @author pedro
 */
public interface IPersonaRepository extends JpaRepository<PersonaDAO, Integer> {
    PersonaDAO findByIdentificacion(Integer id);
    @Transactional
    @Modifying
    @Query("DELETE FROM PersonaDAO AS p WHERE p.id=:id")
    Integer delete(@Param("id") Integer id);
}
