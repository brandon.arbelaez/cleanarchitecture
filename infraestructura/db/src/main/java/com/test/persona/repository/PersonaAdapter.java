package com.test.persona.repository;

import com.google.common.collect.Streams;
import com.test.persona.PersonaEntityMapper;
import com.test.persona.PersonaModel;
import com.test.persona.entity.DAO.PersonaDAO;
import com.test.persona.gateway.PersonaGateway;
import com.test.persona.repository.JPA.IPersonaRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author pedro
 */
@Repository
@RequiredArgsConstructor
public class PersonaAdapter implements PersonaGateway {

    private final IPersonaRepository personaRepository;
    private final PersonaEntityMapper personaEntityMapper;

    @Override
    public List<PersonaModel> findAll() {
        List<PersonaDAO> personaEntities = Streams
                .stream(personaRepository.findAll())
                .collect(Collectors.toList());
        return personaEntityMapper.personaEntityToPersonaModel(personaEntities);
    }

    @Override
    public PersonaModel findByIdentificacion(Integer id) {
        PersonaDAO byIdentificacion = personaRepository.findByIdentificacion(id);
        return personaEntityMapper.personaEntityToPersonaModel(byIdentificacion);
    }

    @Override
    public PersonaModel save(PersonaModel personaModel) {
        PersonaDAO personaDAO = personaEntityMapper.personaModelToPersonaEntity(personaModel);
        PersonaDAO save = personaRepository.save(personaDAO);
        return personaEntityMapper.personaEntityToPersonaModel(save);
    }

    @Override
    public Integer deleteById(Integer id) {
        Integer delete = personaRepository.delete(id);
        System.out.println("");
        return delete;
    }
}
