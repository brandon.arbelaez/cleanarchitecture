package com.test.persona.entity.DAO;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author pedro
 */
@Data
@Entity
@Table(name = "personas")
public class PersonaDAO {

    @Id
    @Column(name = "persona_id", unique = true)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column(name = "persona_nombre")
    private String nombre;

    @Column(name = "persona_apellido")
    private String apellido;

    @Column(name = "persona_identificacion")
    private Integer identificacion;

    @Column(name = "id_tipoidentificacion")
    private Integer tipoDocumento;

    @Column(name = "persona_edad")
    private Integer edad;

    @Column(name = "id_ciudad")
    private Integer ciudad;

}
