package com.test.persona.service;

import com.test.persona.PersonaModel;
import com.test.persona.entity.DTO.PersonaDto;

import java.util.List;

public interface IPersonaService {
    List<PersonaDto> findAll();
    PersonaDto findByIdentificacion(Integer id);
    PersonaModel save(PersonaModel personaModel);
    Integer deleteById(Integer id);
}
