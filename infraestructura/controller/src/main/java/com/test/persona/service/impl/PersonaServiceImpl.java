package com.test.persona.service.impl;

import com.test.imagen.ImagenModel;
import com.test.imagen.ImagenUseCase;
import com.test.imagen.mapper.ImagenDtoMapper;
import com.test.persona.PersonaModel;
import com.test.persona.PersonaUseCase;
import com.test.persona.entity.DTO.PersonaDto;
import com.test.persona.mapper.PersonaDtoMapper;
import com.test.persona.service.IPersonaService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class PersonaServiceImpl implements IPersonaService {
    private final PersonaUseCase personaUseCase;
    private final PersonaDtoMapper personaDtoMapper;
    private final ImagenUseCase imagenUseCase;
    private final ImagenDtoMapper imagenDtoMapper;

    @Override
    public List<PersonaDto> findAll() {
        List<PersonaModel> personaModels = personaUseCase.findAll();
        List<PersonaDto> listDTO = personaDtoMapper.listPersonaModelToPersonaDto(personaModels);
        listDTO.stream().peek(x->{
            x.setImagen(imagenDtoMapper.imagenModelToImagenDto(imagenUseCase.findByPersonaid(String.valueOf(x.getId()))));
        }).collect(Collectors.toList());
        return listDTO;
    }

    @Override
    public PersonaDto findByIdentificacion(Integer id) {
        PersonaModel personaModel= personaUseCase.findByIdentificacion(id);
        PersonaDto personaDto =personaDtoMapper.personaModelToPersonaDto(personaModel);
        ImagenModel byPersonaId = imagenUseCase.findByPersonaid(String.valueOf(personaDto.getId()));
        personaDto.setImagen(imagenDtoMapper.imagenModelToImagenDto(byPersonaId));
        return personaDto;
    }

    @Override
    public PersonaModel save(PersonaModel personaModel) {
        PersonaModel personaModel1=personaUseCase.save(personaModel);
        return personaModel1;
    }

    @Override
    public Integer deleteById(Integer id) {
        Integer delete = personaUseCase.deleteById(id);
        return delete;
    }
}
