package com.test.persona;

import com.test.imagen.ImagenModel;
import com.test.imagen.ImagenUseCase;
import com.test.imagen.mapper.ImagenDtoMapper;
import com.test.persona.entity.DTO.PersonaDto;
import com.test.persona.mapper.PersonaDtoMapper;
import com.test.persona.service.IPersonaService;
import com.test.response.GenericResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author pedro
 */
@RestController
@RequestMapping("/persona")
@RequiredArgsConstructor
public class PersonaControllerImpl implements IPersonaController {
    private final IPersonaService iPersonaService;

    GenericResponseDTO genericResponseDTO = new GenericResponseDTO();


    @Override
    @GetMapping("/listar")
    public ResponseEntity<GenericResponseDTO> findAll() {
        List<PersonaDto> listDTO = iPersonaService.findAll();
        genericResponseDTO.setMessage("Se consultan todas las personas");
        return respuestaGenerica(listDTO);
    }

    @Override
    @GetMapping("/listar/{id}")
    public ResponseEntity<GenericResponseDTO> findByIdentificacion(@PathVariable("id") Integer id) {
        PersonaDto byIdentificacion = iPersonaService.findByIdentificacion(id);
        genericResponseDTO.setMessage("Se consultan la persona por el numero de identificacion: " + id);
        return respuestaGenerica(byIdentificacion);
    }

    @Override
    @PostMapping("/")
    public ResponseEntity<GenericResponseDTO> save(@RequestBody PersonaModel personaModel) {
        PersonaModel personaModel1 = iPersonaService.save(personaModel);
        genericResponseDTO.setMessage("Se guarda la persona con id: " + personaModel1.getId());
        return respuestaGenerica(personaModel1);
    }

    @Override
    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<GenericResponseDTO> deleteById(@PathVariable("id") Integer id) {
        Integer delete = iPersonaService.deleteById(id);
        genericResponseDTO.setMessage(delete > 0 ? "Se elimina la persona con el id: "+id : "No existe la persona");
        return respuestaGenerica(delete);
    }

    protected ResponseEntity<GenericResponseDTO> respuestaGenerica(Object respuestaTramite) {
        return new ResponseEntity<>(GenericResponseDTO.builder().message(genericResponseDTO.getMessage())
                .objectResponse(respuestaTramite).statusCode(HttpStatus.OK.value()).build(), HttpStatus.OK);
    }

}
