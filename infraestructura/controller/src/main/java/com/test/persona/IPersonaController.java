package com.test.persona;

import com.test.response.GenericResponseDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;

public interface IPersonaController {

    ResponseEntity<GenericResponseDTO> findAll();

    ResponseEntity<GenericResponseDTO> findByIdentificacion(Integer id);

    ResponseEntity<GenericResponseDTO> save(@RequestBody PersonaModel personaModel);

    ResponseEntity<GenericResponseDTO> deleteById(Integer id);

}
