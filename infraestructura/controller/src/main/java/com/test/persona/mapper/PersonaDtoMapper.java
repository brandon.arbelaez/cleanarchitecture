package com.test.persona.mapper;

import com.test.persona.PersonaModel;
import com.test.persona.entity.DTO.PersonaDto;
import org.mapstruct.Mapper;

import java.util.List;

/**
 * @author pedro
 */
@Mapper(componentModel = "spring")
public interface PersonaDtoMapper {

    List<PersonaDto> listPersonaModelToPersonaDto(List<PersonaModel> personaModels);
    PersonaDto personaModelToPersonaDto(PersonaModel personaModel);

}
