package com.test.imagen.service.impl;

import com.test.imagen.ImagenModel;
import com.test.imagen.ImagenUseCase;
import com.test.imagen.entity.DTO.ImagenDTO;
import com.test.imagen.mapper.ImagenDtoMapper;
import com.test.imagen.service.ImagenService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ImagenServiceImpl implements ImagenService {
    private final ImagenUseCase imagenUseCase;
    private final ImagenDtoMapper imagenDtoMapper;

    @Override
    public List<ImagenDTO> findAll() {
        List<ImagenModel> imagenModels = imagenUseCase.findAll();
        return imagenDtoMapper.listImagenModelToImagenDto(imagenModels);
    }

    @Override
    public ImagenModel findByPersonaid(String id) {
        ImagenModel imagenModel= imagenUseCase.findByPersonaid(id);
        return imagenModel;
    }

    @Override
    public Integer deleteByPersonaid(String id) {
        Integer delete = imagenUseCase.deleteByPersonaid(id);
        return delete;
    }
}
