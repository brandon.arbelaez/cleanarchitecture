package com.test.imagen.mapper;

import com.test.imagen.ImagenModel;
import com.test.imagen.entity.DTO.ImagenDTO;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface ImagenDtoMapper {
    List<ImagenDTO> listImagenModelToImagenDto(List<ImagenModel> imagenModels);
    ImagenDTO imagenModelToImagenDto(ImagenModel imagenModels);
}
