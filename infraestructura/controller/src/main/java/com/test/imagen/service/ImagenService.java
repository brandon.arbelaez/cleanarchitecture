package com.test.imagen.service;

import com.test.imagen.ImagenModel;
import com.test.imagen.entity.DTO.ImagenDTO;

import java.util.List;

public interface ImagenService {
    List<ImagenDTO> findAll();
    ImagenModel findByPersonaid(String id);
    Integer deleteByPersonaid(String id);

}
