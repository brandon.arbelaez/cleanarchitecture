package com.test.imagen;

import com.test.imagen.entity.DTO.ImagenDTO;
import com.test.response.GenericResponseDTO;
import org.springframework.http.ResponseEntity;

import java.util.List;

public interface ImagenController {
    ResponseEntity<GenericResponseDTO> findAll();
    ResponseEntity<GenericResponseDTO> findByPersonaid(String id);
    ResponseEntity<GenericResponseDTO> deleteByPersonaid(String id);
}
