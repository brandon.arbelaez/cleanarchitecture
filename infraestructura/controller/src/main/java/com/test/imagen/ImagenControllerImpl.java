package com.test.imagen;

import com.test.imagen.entity.DTO.ImagenDTO;
import com.test.imagen.mapper.ImagenDtoMapper;
import com.test.imagen.service.ImagenService;
import com.test.response.GenericResponseDTO;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/imagen")
@RequiredArgsConstructor
public class ImagenControllerImpl implements ImagenController{
    private final ImagenService imagenService;
    GenericResponseDTO genericResponseDTO = new GenericResponseDTO();

    @Override
    @GetMapping("/listar")
    public ResponseEntity<GenericResponseDTO> findAll() {
        List<ImagenDTO> imagenModels = imagenService.findAll();
        genericResponseDTO.setMessage("Se consultan todas las imagenes: ");
        return respuestaGenerica(imagenModels);
    }

    @Override
    @GetMapping("/listar/{id}")
    public ResponseEntity<GenericResponseDTO> findByPersonaid(@PathVariable("id") String id) {
        ImagenModel imagenModel= imagenService.findByPersonaid(id);
        genericResponseDTO.setMessage("Se consulta imagen por el id de la persona: "+id);
        return respuestaGenerica(imagenModel);
    }

    @Override
    @DeleteMapping("/eliminar/{id}")
    public ResponseEntity<GenericResponseDTO> deleteByPersonaid(@PathVariable("id") String id) {
        Integer delete = imagenService.deleteByPersonaid(id);
        return respuestaGenerica(delete);
    }

    protected ResponseEntity<GenericResponseDTO> respuestaGenerica(Object respuestaTramite) {
        return new ResponseEntity<>(GenericResponseDTO.builder().message(genericResponseDTO.getMessage())
                .objectResponse(respuestaTramite).statusCode(HttpStatus.OK.value()).build(), HttpStatus.OK);
    }
}
