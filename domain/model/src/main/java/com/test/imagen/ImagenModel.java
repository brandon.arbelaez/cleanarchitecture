package com.test.imagen;

import lombok.Data;

@Data
public class ImagenModel {
    private String _id;

    private String personaid;

    private String descripcionFoto;
}
