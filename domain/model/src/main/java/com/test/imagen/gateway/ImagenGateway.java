package com.test.imagen.gateway;

import com.test.imagen.ImagenModel;

import java.util.List;

public interface ImagenGateway {
    List<ImagenModel> findAll();
    ImagenModel findByPersonaid(String id);
    Integer deleteByPersonaid(String id);
}
