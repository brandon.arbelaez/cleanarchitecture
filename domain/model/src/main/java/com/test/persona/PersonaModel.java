package com.test.persona;

import lombok.Data;

/**
 * @author pedro
 */
@Data
public class PersonaModel {

    private Integer id;
    private String nombre;
    private String apellido;
    private Integer identificacion;
    private Integer tipoDocumento;
    private Integer edad;
    private Integer ciudad;

}
