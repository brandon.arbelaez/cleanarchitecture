package com.test.persona.gateway;

import com.test.persona.PersonaModel;

import java.util.List;

/**
 * @author pedro
 */
public interface PersonaGateway {

    List<PersonaModel> findAll();
    PersonaModel findByIdentificacion(Integer id);
    PersonaModel save(PersonaModel personaModel);
    Integer deleteById(Integer id);

}
