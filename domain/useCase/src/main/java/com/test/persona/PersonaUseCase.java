package com.test.persona;

import java.util.List;

/**
 * @author pedro
 */
public interface PersonaUseCase {

    List<PersonaModel> findAll();
    PersonaModel findByIdentificacion(Integer id);
    PersonaModel save(PersonaModel personaModel);
    Integer deleteById(Integer id);

}
