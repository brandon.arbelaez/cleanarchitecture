package com.test.imagen.impl;

import com.test.imagen.ImagenModel;
import com.test.imagen.ImagenUseCase;
import com.test.imagen.gateway.ImagenGateway;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class ImagenUseCaseImpl implements ImagenUseCase {
    private final ImagenGateway imagenGateway;

    @Override
    public List<ImagenModel> findAll() {
        return imagenGateway.findAll();
    }

    @Override
    public ImagenModel findByPersonaid(String id) {
        return imagenGateway.findByPersonaid(id);
    }

    @Override
    public Integer deleteByPersonaid(String id) {
        return imagenGateway.deleteByPersonaid(id);
    }
}
