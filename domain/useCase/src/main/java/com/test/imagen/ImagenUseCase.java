package com.test.imagen;

import java.util.List;

public interface ImagenUseCase {
    List<ImagenModel> findAll();
    ImagenModel findByPersonaid(String id);
    Integer deleteByPersonaid(String id);
}
